const fs = require('fs');
require('./dev');
let trucks = require('./truck');
let trailers = require('./trailer');
require('./first');

// Position: 646.17041015625, 3503.772705078125, 33.85976028442383 | punkt startowy 1

let player = mp.players.local;
let trailer;
let vehicle;
let vehicleBlip;
let trailerBlip;
let destBlip;
function start() {
    mp.players.local.call('truckSubtitle');
    const position = new mp.Vector3(646.17041015625, 3503.772705078125, 33.85976028442383);
    vehicleBlip = mp.blips.new(67, position, {
        name: 'Start Point'
    });
    vehicleBlip.routeFor(player, 2, 1);

    vehicle = mp.vehicles.new(
        mp.joaat(trucks[0]),
        new mp.Vector3(646.17041015625, 3500.772705078125, 33.85976028442383),
        {
            numberPlate: 'DEV',
            color: [
                [0, 0, 0],
                [0, 0, 0]
            ]
        }
    );

    let localPlayer = mp.players.local;
    var pos = localPlayer.position;
    const personalVehicle = mp.vehicles.new(
        mp.joaat('oppressor2'),
        new mp.Vector3(pos.x + 2, pos.y, pos.z),
        {
            numberPlate: 'DEV',
            color: [
                [0, 255, 0],
                [0, 255, 0]
            ]
        }
    );

    vehicle.rotation = new mp.Vector3(0, 0, 100);
    console.log(vehicle.rotation.x, vehicle.rotation.y, vehicle.rotation.z);

    
}

let colshape = mp.colshapes.newSphere(644.0925903320312, 3498.65283203125, 33.86107635498047, 3);
mp.events.add('playerEnterColshape', (player, shape) => {
    if (shape == colshape) {
        console.log(`${player.name} entered the colshape`);
        trailerF();
        setTimeout(() => {
            vehicleBlip.destroy();
        }, 1500);
    }
});

let truck = mp.markers.new(
    1,
    new mp.Vector3(644.0925903320312, 3498.65283203125, 33.86107635498047),
    3,
    {
        direction: new mp.Vector3(646.17041015625, 3500.772705078125, 27.85976028442383),
        rotation: 0,
        color: [251, 226, 18, 255], // 212, 83, 0, 255
        visible: true,
        dimension: 0
    }
);

console.log(`Truck marker color: ${truck.getColor()}`);

mp.events.addCommand('start', () => {
    console.log('start');
    start();
});

mp.events.addCommand('log', player => {
    console.log(player.position);
});

function trailerF() {
    mp.players.local.call('trailerSubtitle');
    trailer = mp.vehicles.new(
        mp.joaat(trailers[0]),
        new mp.Vector3(346.2528381347656, 3417.18701171875, 36.30973815917969),
        {
            numberPlate: 'DEV',
            color: [
                [0, 0, 0],
                [0, 0, 0]
            ]
        }
    );

    const position = new mp.Vector3(346.2528381347656, 3417.18701171875, 36.30973815917969);
    trailerBlip = mp.blips.new(479, position, {
        name: 'Trailer pickup'
    });
    trailerBlip.routeFor(player, 29, 1);

    let colshape = mp.colshapes.newSphere(
        346.0755615234375,
        3423.75341796875,
        36.450138092041016,
        10
    );
    mp.events.add('playerEnterColshape', (player, shape) => {
        if (shape == colshape) {
            // console.log(`${player.name} entered the colshape`);
            setTimeout(() => {
                dest();
                trailerBlip.destroy();
            }, 1500);
        }
    });

    let trailerMarker = mp.markers.new(
        1,
        new mp.Vector3(346.2528381347656, 3417.18701171875, 36.30973815917969),
        12,
        {
            direction: new mp.Vector3(646.17041015625, 3500.772705078125, 30.85976028442383),
            rotation: 0,
            color: [212, 83, 0, 255], // 212, 83, 0, 255
            visible: true,
            dimension: 0
        }
    );

    console.log(
        `Trailer marker color: ${trailerMarker.getColor()} | Coordinates: ${trailerMarker.position}`
    );
}

function dest() {
    mp.players.local.call('destinationSubtitle');
    const position = new mp.Vector3(212.68923950195312, 3219.337890625, 42.77021408081055);
    destBlip = mp.blips.new(473, position, {
        name: 'Destination point'
    });
    destBlip.routeFor(player, 29, 1);

    let destinationMarker = mp.markers.new(
        1,
        new mp.Vector3(212.68923950195312, 3219.337890625, 42.77021408081055),
        5,
        {
            direction: new mp.Vector3(646.17041015625, 3500.772705078125, 30.85976028442383),
            rotation: 0,
            color: [212, 83, 0, 255], // 212, 83, 0, 255
            visible: true,
            dimension: 0
        }
    );

    console.log(`Destination Marker color: ${destinationMarker.getColor()}`);
    // -412.89111328125, 1214.44140625, 325.107
    let colshape = mp.colshapes.newSphere(212.68923950195312, 3219.337890625, 42.77021408081055, 5);
    mp.events.add('playerEnterColshape', (player, shape) => {
        if (shape == colshape) {
            player.notify('Reach Waypoint');
            player.removeFromVehicle();
            trailer.destroy();
            vehicle.destroy();
            destBlip.destroy();
        }
    });
}

mp.events.addCommand('pos', player => {
    const pos = player.position;
    const array = pos.toArray();
    console.log(array);
    console.log(array[0]);
});

mp.events.addCommand('in', player => {
    player.outputChatBox('player in vehicle');
    trailer();
});

mp.events.addCommand('attach', player => {
    const position = new mp.Vector3(646.17041015625, 3500.772705078125, 33.85976028442383);
    const blip = mp.blips.new(473, position, {
        name: 'Trailer pickup'
    });
    blip.routeFor(player, 2, 1);
});

mp.events.addCommand('reach', player => {
    blip.unrouteFor(player);
});


mp.events.addCommand('obj', player => {
    mp.objects.new("bkr_prop_fakeid_binbag_01", new mp.Vector3(346.2528381347656, 3417.18701171875, 35.40973815917969)
)});
