mp.events.addCommand('tp', (player, _, x, y, z) => {
    if (!isNaN(parseFloat(x)) && !isNaN(parseFloat(y)) && !isNaN(parseFloat(z)))
        player.position = new mp.Vector3(parseFloat(x), parseFloat(y), parseFloat(z));
    else player.outputChatBox(`<b>Command syntax:</b> /tp [x] [y] [z]`);
});

mp.events.addCommand('veh', (player, _, vehName) => {
    if (vehName && vehName.trim().length > 0) {
        let pos = player.position;
        pos.x += 2;
        // If player has vehicle - change model.
        // if (player.customData.vehicle) {
        //     player.customData.vehicle.repair();
        //     player.customData.vehicle.position = pos;
        //     player.customData.vehicle.model = mp.joaat(vehName);
        // // Else - create new vehicle.
        // } else {
        //     player.customData.vehicle = mp.vehicles.new(mp.joaat(vehName), pos);
        // }
        mp.vehicles.new(mp.joaat(vehName), pos);
    } else {
        player.outputChatBox(`<b>Command syntax:</b> /veh [vehicle_name]`);
    }
});

mp.events.addCommand('veh', (player, text) => {
    if (text == undefined) {
        let veh = mp.vehicles.new(mp.joaat('oppressor2'), player.position);
        player.putIntoVehicle(veh, -1);
    } else {
        let veh = mp.vehicles.new(mp.joaat(text), player.position);
        player.putIntoVehicle(veh, -1);
    }
});
const saveFile = 'savedpos.txt';

mp.events.addCommand('save', (player, name = 'No name') => {
    let pos = player.vehicle ? player.vehicle.position : player.position;
    let rot = player.vehicle ? player.vehicle.rotation : player.heading;

    fs.appendFile(saveFile, `Position: ${pos.x}, ${pos.y}, ${pos.z} | ${name}\r\n`, err => {
        if (err) {
            player.notify(`~r~SavePos Error: ~w~${err.message}`);
        } else {
            player.notify(`~g~Position saved. ~w~(${name})`);
        }
    });
});

mp.events.add('playerJoin', player => {
    player.model = 'mp_m_freemode_01';
    player.spawn(new mp.Vector3(346.2528381347656, 3417.18701171875, 36.30973815917969));
    mp.vehicles.new(mp.joaat('oppressor2'), new mp.Vector3((player.x += 2), player.y, player.z), {
        numberPlate: 'DEV',
        color: [
            [0, 0, 0],
            [0, 0, 0]
        ]
    });

    player.outputChatBox(`[SERVER]: ${player.name} has joined the server!`);
    console.log(`Player position: ${player.position}`)
});

mp.events.add('playerDeath', (player) => {
    player.spawn(new mp.Vector3(346.2528381347656, 3417.18701171875, 36.30973815917969));
    mp.vehicles.new(mp.joaat('oppressor2'), new mp.Vector3(player.position.x + 2, player.position.y, player.position.z), {
        numberPlate: 'DEV',
        color: [
            [0, 0, 0],
            [0, 0, 0]
        ]
    });
});

