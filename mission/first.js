let trucks = require('./truck');

let pounder = trucks[8];
let pounder2 = trucks[9];
let truckVar;

let random = Math.floor(Math.random() * 2);
if (random == 0) {
    truckVar = pounder;
} else if (random == 1) {
    truckVar = pounder2;
}
console.log(`random truck: ${random}, truck var ${truckVar}`);

// player.spawn(spawnPoints[Math.floor(Math.random() * spawnPoints.length)]);
let player = mp.players.local;
let veh;
let vehBlip;
let destBlip;

// const startPos = {
//     X: 909.3377075195312,
//     Y: -1566.14306640625,
//     Z: 30.73444175720215
// };
// , , 
const startPos = {
    X: 2856.865966796875,
    Y: 4414.4306640625,
    Z: 48.95960235595703
};

const destPos = {
    X: 1410.210693359375,
    Y: 1044.317138671875,
    Z: 114.19927978515625
};

const blips = {
    truck: 477,
    trailer: 479,
    destination: 474
};

mp.events.addCommand('first', player => {
    setTimeout(() => {
        init();
        player.notify('Go pickup a ~y~ truck')
    }, 2000);

    player.setWantedLevelNow(false);
    // init();
    let playerVehicle = 'cheburek';
    mp.vehicles.new(
        mp.joaat(playerVehicle),
        new mp.Vector3(player.position.x + 2, player.position.y, player.position.z),
        {
            numberPlate: 'DEV',
            color: [
                [0, 255, 0],
                [0, 255, 0]
            ]
        }
    );
});

function init() {
    // mp.players.local.call('truckSubtitle');
    
    
    const truckPosition = new mp.Vector3(startPos.X, startPos.Y, startPos.Z);
    vehBlip = mp.blips.new(blips.truck, truckPosition, {
        name: 'Truck pickup'
    });
    vehBlip.routeFor(player, 46, 1);

    veh = mp.vehicles.new(mp.joaat(truckVar), new mp.Vector3(startPos.X, startPos.Y, startPos.Z), {
        numberPlate: 'DEV',
        color: [
            [0, 0, 0],
            [0, 0, 0]
        ]
    });

    veh.rotation = new mp.Vector3(0, 0, 20);

    let colshape = mp.colshapes.newSphere(startPos.X, startPos.Y, startPos.Z, 3);
    mp.events.add('playerEnterColshape', (player, shape) => {
        if (shape == colshape) {
            console.log(`${player.name} entered the colshape`);
            vehBlip.destroy();
            player.notify('Go to the ~b~destination')
            mp.players.local.call('destroy');
            setTimeout(() => {
                destination();
            }, 2000);
        }
    });
}

function destination() {
    
    mp.players.local.call('destinationSubtitle');
    const destPosition = new mp.Vector3(destPos.X, destPos.Y, destPos.Z);
    destBlip = mp.blips.new(blips.destination, destPosition, {
        name: 'Destination'
    });
    destBlip.routeFor(player, 38, 1);

    let colshape = mp.colshapes.newSphere(destPos.X, destPos.Y, destPos.Z, 3);
    mp.events.add('playerEnterColshape', (player, shape) => {
        if (shape == colshape) {
            console.log(`${player.name} entered the colshape`);
            player.notify('Reach Waypoint');
            player.removeFromVehicle();
            setTimeout(() => {
                veh.destroy();
                destinationMarker.destroy();
                destBlip.destroy();
            }, 2000);
        }
    });

    let destinationMarker = mp.markers.new(1, new mp.Vector3(destPos.X, destPos.Y, destPos.Z), 12, {
        direction: new mp.Vector3(646.17041015625, 3500.772705078125, 30.85976028442383),
        rotation: 0,
        color: [212, 83, 0, 255], // 212, 83, 0, 255
        visible: true,
        dimension: 0
    });
}
