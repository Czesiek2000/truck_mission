const trailers = [
    'docktrailer',
    'freighttrailer',
    'tanker',
    'tanker2',
    'trailerlarge',
    'trflat',
    'tr2',
    'tr3',
    'tr4',
    'trflat',
    'tvtrailer',
    'trailerlogs',
    'trailers',
    'trailers2',
    'trailers3',
    'trailers4'
];

module.exports = trailers;
